function loadImage(imageUrl: string): Promise<HTMLImageElement> {
	return new Promise((resolve) => {
		const $img = new Image();
		$img.src = imageUrl;
		$img.crossOrigin = 'Anonymous';
		$img.onload = () => {
			resolve($img);
		};
	});
}
export const useColor = () => {
	return {
		// https://stackoverflow.com/a/2541680
		async getAverageRGB(imageUrl: string) {
			if (!imageUrl) return { r: 0, g: 0, b: 0 };
			const $img = await loadImage(imageUrl);
			const blockSize = 5;
			const defaultRGB = { r: 0, g: 0, b: 0 };
			const canvas = document.createElement('canvas');
			const context = canvas.getContext && canvas.getContext('2d');
			let data;
			let i = -4;
			const rgb: { [index: string]: number } = { r: 0, g: 0, b: 0 };
			let count = 0;

			if (!context) {
				return defaultRGB;
			}

			const height = (canvas.height =
				$img.naturalHeight || $img.offsetHeight || $img.height);
			const width = (canvas.width =
				$img.naturalWidth || $img.offsetWidth || $img.width);

			context.drawImage($img, 0, 0);

			try {
				data = context.getImageData(0, 0, width, height);
			} catch (e) {
				/* security error, img on diff domain */
				return defaultRGB;
			}

			const length = data.data.length;

			while ((i += blockSize * 4) < length) {
				++count;
				rgb.r += data.data[i];
				rgb.g += data.data[i + 1];
				rgb.b += data.data[i + 2];
			}

			// ~~ used to floor values
			rgb.r = ~~(rgb.r / count);
			rgb.g = ~~(rgb.g / count);
			rgb.b = ~~(rgb.b / count);

			return rgb;
		},
	};
};
