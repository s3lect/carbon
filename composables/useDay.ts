import * as dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
dayjs.extend(relativeTime);

export const useDay = () => {
	return {
		fromNow(date: number) {
			return dayjs().to(dayjs(date));
		},
		hhmm(date: number) {
			return dayjs(date).format('hh:mm');
		},
	};
};
