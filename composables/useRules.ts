const domainRegEx = /\w{2,}\.\w{2,}/;
export const useRules = () => {
	return {
		required: (value: any) => !!value || 'Input is required',
		isDomain(value: string) {
			return domainRegEx.test(value) || 'Instance domain not valid';
		},
	};
};
