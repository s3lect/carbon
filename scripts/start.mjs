#!/usr/bin/env node
/* eslint no-console: "off" */
import { execSync } from 'child_process';
import inquirer from 'inquirer';
import autocomplete from 'inquirer-autocomplete-prompt';
import Fuse from 'fuse.js';
import packageJson from '../package.json' assert { type: 'json' };
import { checkAndUpdatePackageJson } from './install.mjs';
inquirer.registerPrompt('autocomplete', autocomplete);

async function run() {
	checkAndUpdatePackageJson();

	// Get all scripts from package.json
	// exclude `start` since this runs this script
	const actions = Object.entries(packageJson.scripts)
		.filter(([name]) => name !== 'start')
		.map(([name, value]) => ({
			name,
			callback: () => {
				execSync(value, { stdio: [0, 1, 2] });
			},
		}));

	const fuse = new Fuse(actions, { keys: ['name'] });
	const { startAction } = await inquirer.prompt([
		{
			type: 'autocomplete',
			name: 'startAction',
			message: 'What do you want to do?',
			default: 'dev',
			source: (_, input) => {
				if (!input) return actions.map(({ name }) => name);
				return fuse.search(input).map(({ item }) => item.name);
			},
		},
	]);

	await actions.find(({ name }) => name === startAction).callback();
}

run().then(() => console.log('The End.'));
