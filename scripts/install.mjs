#!/usr/bin/env node
/* eslint no-console: "off" */
import { readFile, writeFile } from 'fs/promises';
import { execSync } from 'child_process';
import url from 'url';
import { join } from 'path';

const nodejsMajorVerion = parseInt(process.version.match(/^v(\d+)\./)[1], 10);
if (nodejsMajorVerion < 19) {
	console.error(
		`The Node.js version needs to be >= 19 but it is ${nodejsMajorVerion}.`
	);
	process.exit(1);
}

function hashCode(inputString) {
	let hash = 0;
	if (inputString.length === 0) return hash;
	for (const char of inputString) {
		hash = (hash << 5) - hash + char.charCodeAt(0);
		hash = hash & hash; // Convert to 32bit integer
	}
	return hash;
}

export async function checkAndUpdatePackageJson() {
	const __dirname = url.fileURLToPath(new URL('.', import.meta.url));
	// Check if package.json changed and run `yarn install` if it changed.
	const packageJson = await import('../package.json', {
		assert: { type: 'json' },
	});
	// const packageJson = JSON.parse(readFile(join(__dirname, '..', 'package.json'), 'utf8'));
	const currentHash = `${hashCode(
		JSON.stringify([packageJson.dependencies, packageJson.devDependencies])
	)}`;
	let dependenciesHash = '';
	try {
		dependenciesHash = await readFile(
			join(__dirname, '..', 'package.hash'),
			'utf8'
		);
	} catch {
		// no worries we will write it
	}
	if (dependenciesHash !== currentHash) {
		console.log(
			'The package.json hash has changed, will now run "yarn install" to update dependencies.'
		);
		dependenciesHash = await writeFile(
			join(__dirname, '..', 'package.hash'),
			currentHash
		);
		execSync('yarn install', {
			stdio: [0, 1, 2],
		});
	}
}

checkAndUpdatePackageJson().catch((e) => console.log(e));
