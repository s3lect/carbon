export const useMedia = defineStore('media', () => {
	const isActive = ref(false);
	return {
		isActive,
	};
});
