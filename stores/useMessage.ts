import type { Ref } from 'vue';

interface IMessage {
	body: string;
	type: string;
}

export const useMessage = defineStore('message', () => {
	const currentMessage: Ref<IMessage | null | undefined> = ref(null);
	const messagQueue: IMessage[] = [];
	return {
		currentMessage,
		showMessage(message: IMessage) {
			if (!currentMessage.value) currentMessage.value = message;
			else messagQueue.push(message);
		},
		closeMessage() {
			currentMessage.value = null;
			// If there are more messages on the queue remove one and show it
			// after a small delay.
			if (messagQueue.length) {
				setTimeout(() => {
					currentMessage.value = messagQueue.shift();
				}, 250);
			}
		},
	};
});
