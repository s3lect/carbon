import type {
	MatrixClient,
	Room,
	MatrixEvent,
	EmittedEvents,
} from 'matrix-js-sdk';

declare let matrixcs: any;

// Convert output from matrix `loginWithPassword` and `registerGuest` to
// the input needed for `createClient`
const refineCredentials = (
	baseUrl: string,
	_credentials: IMatrixCredentials
) => ({
	baseUrl,
	accessToken: _credentials.access_token,
	userId: _credentials.user_id,
	deviceId: _credentials.device_id,
});

// let matrixClient: MatrixClient = null as MatrixClient;
let matrixClient: MatrixClient = null as never;
// let matrixClient: MatrixClient = null as any;

export const useMatrix = defineStore('matrix', () => {
	const { showMessage } = useMessage();

	const credentials = useRuntimeConfig().MATRIX_CREDENTIALS;
	const connectionState = ref<'disconnect' | 'connect' | 'pending'>(
		'disconnect'
	);
	const userIdToProfile = reactive<{ [index: string]: IProfile }>({});
	const roomById = reactive<{ [index: string]: IRoom }>({});
	const rooms = ref<IRoom[]>([]);
	const profile = reactive<IProfile>({
		userId: credentials.userId,
		displayName: '',
		avatarUrl: '',
	});

	function stopClient() {
		matrixClient.stopClient();
		connectionState.value = 'disconnect';
		showMessage({ body: 'Matrix stopped.', type: 'info' });
	}

	let syncErrorCount = 0;

	const eventHandlers: { [index: string]: (data: any) => void } = {
		'Room.timeline'(event: MatrixEvent) {
			const roomId = event.getRoomId();
			if (roomId && roomId in roomById) roomById[roomId].refreshCounter++;
		},
		sync(syncState: string) {
			const handlers: any = {
				ERROR() {
					if (syncErrorCount >= 3) {
						showMessage({
							body: 'Connect retry failed. Disconnecting.',
							type: 'error',
						});
						stopClient();
					} else {
						showMessage({
							body: 'Could not connect to matrix server.',
							type: 'error',
						});
						syncErrorCount++;
					}
				},
				SYNCING() {
					// TODO update UI to remove any "Connection Lost" message
					syncErrorCount = 0;
				},
				PREPARED() {
					connectionState.value = 'connect';
					getProfileInfo(credentials.userId);
				},
			};
			if (syncState in handlers) handlers[syncState]();
		},
	};

	async function getProfileInfo(userId: string) {
		const result = await matrixClient.getProfileInfo(userId);
		Object.assign(profile, result);
		if (result.avatar_url) {
			profile.avatarUrl = matrixClient.mxcUrlToHttp(result.avatar_url, 40, 40);
		}
	}

	function getRoomData(mxroom: Room) {
		return {
			roomId: mxroom.roomId,
			name: mxroom.name,
			numMembers: mxroom.getMembers().length,
			avatarUrl:
				mxroom.getAvatarUrl(matrixClient.getHomeserverUrl(), 20, 20, 'scale') ||
				undefined,
			mxroom,
			timeline: mxroom.getTimelineSets()[0].getTimelines()[0],
			refreshCounter: 0,
		};
	}

	async function fetchRelations(
		roomId: string,
		eventId: string,
		relationType?: string
	) {
		const { chunk } = await matrixClient.fetchRelations(
			roomId,
			eventId,
			relationType
		);
		return chunk;
	}

	async function joinRoom(roomIdOrAlias: string) {
		try {
			const mxroom = await matrixClient.joinRoom(roomIdOrAlias);
			roomById[mxroom.roomId] = getRoomData(mxroom);
		} catch (error: any) {
			showMessage({ body: error.message, type: 'error' });
		}
	}

	function sendMessage(roomId: string, text: string) {
		return matrixClient.sendTextMessage(roomId, text);
	}

	function getUrlPreview(url: string, ts: number) {
		return matrixClient.getUrlPreview(url, ts);
	}

	function getUserProfile(userId: string, roomId?: string) {
		if (userId in userIdToProfile) return userIdToProfile[userId];
		const user = matrixClient.getUser(userId);
		if (!user) return;
		const avatarUrl = user.avatarUrl
			? matrixClient.mxcUrlToHttp(user.avatarUrl, 20, 20)
			: undefined;

		const profile: IProfile = {
			userId,
			avatarUrl,
			displayName: user.displayName,
		};
		userIdToProfile[userId] = profile;
		if (avatarUrl) {
			const { getAverageRGB } = useColor();
			getAverageRGB(avatarUrl).then((rgb) => {
				userIdToProfile[userId].averageRGB = [...'rgb']
					.map((color: string) => rgb[color])
					.join(',');
				if (roomId) roomById[roomId].refreshCounter++;
			});
		}
		return profile;
	}

	function getImageUrl(mxcUrl: string, size?: number) {
		return matrixClient.mxcUrlToHttp(mxcUrl, size, size) || undefined;
	}

	async function paginate(roomId: string) {
		const room = matrixClient.getRoom(roomId);
		if (!room) throw new Error('Could not get room');
		const isEndOfTimeline = await matrixClient.paginateEventTimeline(
			room.getTimelineSets()[0].getTimelines()[0],
			{ backwards: true }
		);
		if (isEndOfTimeline) {
			console.log('this is the end, my only friend the end');
		}
	}

	function getRoom(roomId: string) {
		if (roomId in roomById) return roomById[roomId];
		const mxroom = matrixClient.getRoom(roomId);
		if (!mxroom) return;
		const room = getRoomData(mxroom);
		roomById[roomId] = room;
		rooms.value.push(room);
		return room;
	}

	function loadRooms() {
		matrixClient.getRooms().map((mxroom) => getRoom(mxroom.roomId));
	}

	async function getCredentialsWithPassword(
		baseUrl: string,
		username: string,
		password: string
	) {
		try {
			return refineCredentials(
				baseUrl,
				await matrixcs
					.createClient({ baseUrl })
					.loginWithPassword(username, password)
			);
		} catch (error: any) {
			showMessage({ body: error.message, type: 'error' });
		}
	}

	async function login(_credentials?: ICredentials, isGuest?: boolean) {
		connectionState.value = 'pending';
		const store = new matrixcs.IndexedDBStore({
			indexedDB: window.indexedDB,
			localStorage: window.localStorage,
		});
		await store.startup(); // load from indexed db
		matrixClient = matrixcs.createClient({
			...(_credentials || credentials),
			store,
			// cryptoStore: new LocalStorageCryptoStore(localStorage),
			timelineSupport: true,
		});
		if (!matrixClient) {
			showMessage({ body: 'Could not create client', type: 'error' });
			connectionState.value = 'disconnect';
			return;
		}
		if (isGuest) matrixClient.setGuest(true);
		// Connect event handlers
		Object.entries(eventHandlers).forEach(([eventName, handler]) => {
			// FIXME typscript annotation is bullshit, not sure how it really works here
			matrixClient.on(eventName as EmittedEvents, handler);
		});
		// matrixClient.initCrypto();
		matrixClient.startClient({ initialSyncLimit: 0 });
	}

	return {
		credentials,
		connectionState,
		profile,
		roomById,
		rooms,

		getRoom,
		loadRooms,
		getRoomData,
		fetchRelations,
		getProfileInfo,
		joinRoom,
		sendMessage,
		getUrlPreview,
		getUserProfile,
		paginate,
		getCredentialsWithPassword,
		login,
		getImageUrl,
		stopClient,
	};
});
