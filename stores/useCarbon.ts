import type { MatrixEvent } from 'matrix-js-sdk';
import { watchDebounced } from '@vueuse/core';

const urlRexEx = /(www|http:|https:)+[^\s]+[\w]/g;

function getDomain(url: string) {
	const urlObject = new URL(url);
	return urlObject.hostname;
}

export const useCarbon = defineStore('carbon', () => {
	// Create initial list of timeline events to render
	const { roomById, getUrlPreview, getImageUrl } = useMatrix();
	const events = ref<ICarbonEvent[]>([]);
	const eventById: { [index: string]: ICarbonEvent } = {};
	const processingQueue: { url: string; event: ICarbonEvent }[] = [];
	const urlToPreview: { [index: string]: any } = {};

	/**
	 * Add a URL preview to a carbon event.
	 *
	 * Since some previews can already be cached this function is used to attach the URL preview to the carbon event.
	 *
	 * The previews are retrieved in `processNextUrl`
	 */
	function addUrlPreviewToEvent(
		url: string,
		event: ICarbonEvent,
		preview: any
	) {
		if (!event.urlPreviews.some((ep) => preview.url === ep.url))
			event.urlPreviews.push({ url, ...preview });
	}
	let processingActive = false;
	/** Process the next URL on the preview processing queue.
	 *
	 * The URL preview is retrieved from the matrix server and
	 * extended with: the URL itself, the domain, the external preview image URL if available. */
	function processNextUrl() {
		if (!processingQueue.length || processingActive) return;
		const { url, event }: { url: string; event: ICarbonEvent } =
			processingQueue.pop()!;
		processingActive = true;
		setTimeout(async () => {
			try {
				urlToPreview[url] = await getUrlPreview(url, event!.mxevent.getTs());
				urlToPreview[url].domain = getDomain(url);
				if (urlToPreview[url]['og:image']) {
					urlToPreview[url].imageSrc = getImageUrl(
						urlToPreview[url]['og:image'],
						event.mxevent.getTs()
					);
				}
				addUrlPreviewToEvent(url, event, urlToPreview[url]);
			} catch {}
			processingActive = false;
			processNextUrl();
		}, 500);
	}

	/** Add urls to url preview processing queue. */
	function queueUrls(urls: string[], event: ICarbonEvent) {
		urls.forEach((url) => {
			if (url in urlToPreview)
				addUrlPreviewToEvent(url, event, urlToPreview[url]);
			else processingQueue.push({ url, event });
		});
		processNextUrl();
	}

	/** Convert a list of matrix events to a reduced list of carbon events.
	 *
	 *  Some events like room membership changes should be condensed into one element in the timeline.
	 *  Other events are relations like "reactions" and should be attached to the target event of the relation.
	 *
	 * 	The orinial matrix event is also stored as `mxevent` since most components are rendered using the matrix-js-sdk function.
	 */
	function convertEvents(mxevents: MatrixEvent[]) {
		const _events: ICarbonEvent[] = [];
		for (const mxevent of mxevents) {
			if (mxevent.isRedaction()) continue;
			const type = mxevent.getType();
			if (mxevent.isRelation()) {
				if (type === 'm.room.message') continue;
				if (type === 'm.reaction') {
					if (mxevent.relationEventId && mxevent.relationEventId in eventById) {
						const { key } = mxevent.getContent();
						if (key) eventById[mxevent.relationEventId].reactions.push(key);
					}
					continue;
				}
			}

			const eventId = mxevent.getId();
			// const roomId = mxevent.getRoomId();
			if (!eventId) return;
			// const relations = await fetchRelations(roomId, eventId/*, 'm.reaction'*/);
			// if (relations.length) console.log('relations', relations);
			// const reactions = relations.map((mxevent) => mxevent.content.key);
			// if (reactions.length) console.log('reactions', reactions);
			const newEvent = reactive<ICarbonEvent>({
				id: eventId,
				type,
				mxevent,
				reactions: [],
				urls: [],
				urlPreviews: [],
			});
			eventById[eventId] = newEvent;
			const prevEvent = _events[_events.length - 1];
			if (newEvent.type === 'm.room.message') {
				const { msgtype, body } = mxevent.getContent();
				if (msgtype === 'm.text') {
					let match = urlRexEx.exec(body);
					while (match !== null) {
						newEvent.urls.push(match[0]);
						match = urlRexEx.exec(body);
					}
					queueUrls(newEvent.urls, newEvent);
				}
			}
			// Group room member events so they can be displayed as inline list.
			// If the previous event was also a room member event add this event to the previous
			// and not to the root list
			if (newEvent.type === 'm.room.member') {
				if (prevEvent && prevEvent.type === newEvent.type) {
					prevEvent.events!.push(newEvent);
					continue;
				} else {
					// The first element in the list is itself so the group is initialized.
					newEvent.events = [newEvent];
				}
			}
			_events.push(newEvent);
		}
		return _events;
	}

	let stopListener: () => void | undefined;
	function getRoom(roomId: string) {
		if (stopListener) stopListener();
		const room = useMatrix().getRoom(roomId);
		events.value = [];
		if (!room) return { room, events };
		stopListener = watchDebounced(
			() => room.refreshCounter,
			() => {
				events.value = convertEvents(room.timeline.getEvents())!;
			},
			{ debounce: 500, maxWait: 1000 }
		);
		events.value = convertEvents(room.timeline.getEvents())!;
		return { room, events };
	}

	return {
		getRoom,
		roomById,
		convertEvents,
	};
});
