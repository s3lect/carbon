export default defineNuxtRouteMiddleware(() => {
	const { isMainMenuActive } = storeToRefs(useUi());
	isMainMenuActive.value = false;
});
