export default defineNuxtPlugin((nuxtApp) => {
	nuxtApp.hook('app:beforeMount', () => {
		const { login, credentials } = useMatrix();
		login(credentials).catch((e) => window.console.error(e));
	});
});
