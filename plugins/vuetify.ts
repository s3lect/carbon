import { createVuetify, ThemeDefinition } from 'vuetify';
// import { aliases, mdi } from 'vuetify/iconsets/mdi-svg'
import { aliases, mdi } from 'vuetify/iconsets/mdi';
import * as components from 'vuetify/components';
import * as directives from 'vuetify/directives';

const dark: ThemeDefinition = {
	dark: true,
	colors: {
		background: '#393939',
		surface: '#525252',
		'on-background': '#f4f4f4',

		primary: '#ffb703',
		secondary: '#2d3047',

		error: '#B00020',
		info: '#525252',
		success: '#4CAF50',
		warning: '#FB8C00',
	},
};
const light: ThemeDefinition = {
	dark: false,
	colors: {
		background: '#f4f4f4',
		surface: '#e2e2e2',
		'on-background': '#393939',

		primary: '#ffb703',
		secondary: '#a5402d',

		error: '#B00020',
		info: '#525252',
		success: '#4CAF50',
		warning: '#FB8C00',
	},
};

export default defineNuxtPlugin((nuxtApp) => {
	const vuetify = createVuetify({
		components,
		directives,
		theme: {
			defaultTheme: 'dark',
			themes: {
				dark,
				light,
			},
		},
		defaults: {
			VBtn: {
				color: 'primary',
				variant: 'flat',
			},
		},
		icons: {
			defaultSet: 'mdi',
			aliases,
			sets: {
				mdi,
			},
		},
	});

	nuxtApp.vueApp.use(vuetify);
});
