import MarkdownIt from 'markdown-it';
const md = new MarkdownIt({ linkify: true });
export default defineNuxtPlugin(() => {
	return { provide: { md: (markdownText: string) => md.render(markdownText) } };
});
