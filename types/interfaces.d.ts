interface ICredentials {
	baseUrl: string;
	accessToken: string;
	userId: string;
	deviceId: string;
}
interface IMatrixCredentials {
	access_token: string;
	user_id: string;
	device_id: string;
}
interface IRoom {
	name: string;
	roomId: string;
	numMembers: number;
	avatarUrl?: string;
	mxroom?: Room;
	timeline: EventTimeline;
	refreshCounter: number;
}
interface IProfile {
	avatarUrl: string | null | undefined;
	userId: string;
	displayName?: string;
	averageRGB?: string;
}

interface ICarbonEvent {
	type: string;
	id: string;
	mxevent: MatrixEvent;
	events?: ICarbonEvent[];
	reactions: string[];
	urls: string[];
	urlPreviews: any[];
}
