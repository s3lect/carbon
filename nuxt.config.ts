import vuetify from 'vite-plugin-vuetify';

export default defineNuxtConfig({
	app: {
		head: {
			title: 'Carbon IM',
			script: [{ src: '/js/browser-matrix.min.js' }],
			link: [{ rel: 'manifest', href: '/carbon.webmanifest' }],
		},
	},
	devServer: {
		host: '0.0.0.0',
	},
	runtimeConfig: {
		public: {
			MATRIX_CREDENTIALS: process.env.MATRIX_CREDENTIALS
				? JSON.parse(process.env.MATRIX_CREDENTIALS)
				: undefined,
		},
	},
	modules: [
		// @ts-ignore
		// this adds the vuetify vite plugin
		// also produces type errors in the current beta release
		(_, nuxt) => {
			nuxt.hooks.hook('vite:extendConfig', (config) => {
				config.plugins!.push(vuetify());
			});
		},
		['@pinia/nuxt', { autoImports: ['storeToRefs', 'defineStore'] }],
	],
	css: [
		// 'vuetify/styles',
		'~/assets/vuetify-settings.scss',
		'~/assets/styles.sass',
		'@mdi/font/css/materialdesignicons.css',
	],
	imports: {
		// Auto-import pinia stores defined in `~/stores`
		dirs: ['stores'],
	},
	vite: {
		// @ts-ignore
		// curently this will lead to a type error, but hopefully will be fixed soon #justBetaThings
		ssr: { noExternal: ['vuetify'] },
	},
});
