# carbon

Carbon is a matrix client with flexible integrations to

TODO: Badges

TODO Visuals (add some screenshots once there is something to see)

## Description

TODO

## Setup

Make sure to install the dependencies:

```bash
# yarn
yarn install

# npm
npm install

# pnpm
pnpm install
```

## Development Server

Start the development server on http://localhost:3000

```bash
npm run dev
```

## Production

Build the application for production:

```bash
npm run build
```

Locally preview production build:

```bash
npm run preview
```

## Usage

Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

### The `matrix://` protocol handler

```
matrix:[//{authority}/]{type}/{id without sigil}[/{type}/{id without sigil}...][?{query}][#{fragment}]
```

https://github.com/matrix-org/matrix-spec-proposals/blob/611afff7848c52459826531db467b8b68ccb93af/content/appendices.md#uris

### The `podcast://` protocol handler

## Support

Issues: https://gitlab.com/s3lect/carbon/-/issues
Community: matrix channel here

## Roadmap

If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing

State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment

- Vue
- Nuxt
- Vuetify
- day.js
- vueuse
- [markdown-it](https://github.com/markdown-it/markdown-it)
- [gemoji](https://github.com/github/gemoji/blob/master/db/emoji.json)
- [vue3datepicker](https://vue3datepicker.com/)

### Inspiration

- OneView Calendar https://play.google.com/store/apps/details?id=com.oneviewcalendar.app&hl=en&gl=US

## License

For open source projects, say how it is licensed.

## Project status

If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
